defmodule Quanterall.Service.Client do
  alias Quanterall.Users.Schemas.VerificationRequest
  alias Quanterall.UA
  require Logger

  alias Quanterall.Users.{
    Verifications.Requests
  }

  alias __MODULE__.Payload

  @verification_types [:service, :service_doc]

  def check_verification(record_id, type) when type in @verification_types do
    type
    |> build_session()
    |> call_api(:get, transaction_record_path(record_id), "")
  end

  def check_verification(_, _), do: {:error, :invalid_verification_type}

  def verify(%VerificationRequest{} = request) do
    with {:ok, _} <- verify_and_update(:service, request),
         {:ok, vr} <- verify_and_update(:service_doc, request) do
      {:ok, vr}
    else
      error ->
        Logger.error("Service error while updating verification request",
          event: %{error: error}
        )

        {:error, error}
    end
  end

  defp verify_and_update(type, request) do
    payload =
      case type do
        :service ->
          Payload.idv_payload(request)

        :service_doc ->
          Payload.docv_payload(request)
      end

    with {:ok, %{body: %{"TransactionID" => tx_id}}} <- verify_api_call(payload, type),
         {:ok, vr} <- Requests.resubmit(request, %{"#{type}_transaction_id" => tx_id}) do
      {:ok, vr}
    end
  end

  defp verify_api_call(body, type) do
    type
    |> build_session()
    |> call_api(:post, verify_path(), body, [])
  end

  @service Application.get_env(:quanterall, :service_client, UA)
  @service_endpoint Application.get_env(:quanterall, :service_endpoint)

  defp call_api(session, method, path, body, opts \\ []) do
    case @service.perform(session, method, path, body, opts) do
      {:ok, %UA.Result{body: body} = result} ->
        {:ok, Map.put(result, :body, Jason.decode!(body))}

      err ->
        err
    end
  end

  defp build_session(type) do
    UA.session(%{
      base_url: @service_endpoint,
      timeout: 65_000,
      conn_timeout: 65_000,
      recv_timeout: 65_000,
      headers: [
        {"Content-Type", "application/json"},
        {"Accept", "application/json"},
        {"Authorization", "Basic #{config(type)}"}
      ]
    })
  end

  @type_to_token %{service: "idv", service_doc: "docv"}

  defp config(type) do
    tokens =
      :service
      |> Quanterall.Vault.get_secret()
      |> Enum.into(%{})

    Map.get(tokens, String.to_atom("access_token_#{@type_to_token[type]}"))
  end

  defp transaction_record_path(uuid) do
    "/verify/v1/transaction/#{uuid}"
  end

  defp verify_path, do: "/verify/v1/transaction"
end
