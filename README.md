# Elixir code snippets

This repository serves as an **example** on how we write Elixir at Quanterall.

### Notice

Keep in mind that some of the Modules/Variables names are changed so the code may not be executable.

## Quanterall

See the [Quanterall Website](https://quanterall.com/) for more information about the company.
