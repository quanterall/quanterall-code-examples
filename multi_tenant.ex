defmodule Service.Repo do
  use Ecto.Repo,
    otp_app: :service,
    adapter: Ecto.Adapters.Postgres

  def as_company(company_id, txn) when is_function(txn) do
    transaction(fn ->
      Service.Repo.query("SELECT set_config('app.company_id', $1, true)", [
        to_string(company_id)
      ])

      Service.Repo.query("SELECT set_config('app.restriction_disabled', $1, true)", [
        "false"
      ])

      txn.()
    end)
  end

  def with_disabled_restriction(txn) when is_function(txn) do
    transaction(fn ->
      Service.Repo.query("SELECT set_config('app.company_id', '', true)")
      Service.Repo.query("SELECT set_config('app.restriction_disabled', 'true', true)")

      txn.()
    end)
  end
end
