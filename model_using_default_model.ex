defmodule AuthService.User do
  use AuthService.DefaultModel

  @required_fields [:password, :email, :system_id]
  @derive {Jason.Encoder, only: [:id, :email, :system, :system_id]}
  schema "users" do
    field(:email, :string, required: true)
    field(:password, :string, required: true)
    belongs_to(:system, AuthService.System)

    timestamps()
  end

  @doc false
  def changeset(struct, attrs \\ %{}) do
    struct
    |> cast(attrs, @required_fields, [])
    |> validate_required([:password])
    |> validate_length(:password, min: 6, message: "should_be_at_least_6_characters")
    |> validate_length(:password, max: 32, message: "should_be_at_most_32_characters")
    |> validate_confirmation(:password, message: "does_not_match")
    |> unique_constraint(:email, name: :users_email)
    |> foreign_key_constraint(:system_id, name: :users_system_id_fkey)
    |> put_password_hash()
  end

  defp put_password_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset
       ) do
    put_change(changeset, :password, Bcrypt.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset) do
    changeset
  end
end
