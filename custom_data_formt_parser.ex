defmodule Edi.Parser do
  require Logger
  alias Edi.ParserDataFormater
  alias Edi.ParserUpdater

  # @callback transform(String.t()) :: String.t()

  defmacro __using__(_param) do
    quote do
      # @behaviour Edi.Parser

      @doc ~S"""
      RECORD 00: HEAD RECORD FIELD1.
      """
      def parse_record(
            <<
              "00",
              type::binary-size(3),
              description::binary-size(20),
              payer_comm_charges::binary-size(2),
              sender_code::binary-size(8),
              receiver_code_1::binary-size(8),
              receiver_code_2::binary-size(8),
              receiver_code_3::binary-size(8),
              receiver_code_4::binary-size(8),
              send_time::binary-size(10),
              send_time_tag::binary-size(1),
              sender_port_code::binary-size(5),
              receiver_port_code_1::binary-size(5),
              receiver_port_code_2::binary-size(5),
              receiver_port_code_3::binary-size(5),
              receiver_port_code_4::binary-size(5),
              creator_code::binary-size(8),
              creator_port_code::binary-size(5),
              version::binary-size(3),
              option_port::binary-size(1),
              _::binary-size(8)
            >>,
            acc,
            data
          ) do
        receiver_codes =
          [receiver_code_1, receiver_code_2, receiver_code_3, receiver_code_4]
          |> ParserDataFormater.edi_string_list()

        receiver_port_codes =
          [receiver_port_code_1, receiver_port_code_2, receiver_port_code_3, receiver_port_code_4]
          |> ParserDataFormater.edi_string_list()

        send_time_dt = ParserDataFormater.edi_mandatory_datetime(send_time)

        header = %{
          tag: 0,
          type: ParserDataFormater.edi_string(type),
          description: ParserDataFormater.edi_string(description),
          payer_comm_charges: ParserDataFormater.edi_string(payer_comm_charges),
          sender_code: ParserDataFormater.edi_string(sender_code),
          receiver_codes: receiver_codes,
          receiver_port_codes: receiver_port_codes,
          send_time: send_time_dt,
          send_time_tag: send_time_tag,
          sender_port_code: ParserDataFormater.edi_string(sender_port_code),
          creator_code: ParserDataFormater.edi_string(creator_code),
          creator_port_code: ParserDataFormater.edi_string(creator_port_code),
          version: ParserDataFormater.edi_string(version),
          option_port: ParserDataFormater.edi_boolean(option_port)
        }

        ParserUpdater.update_header(acc, header)
        |> callback("00", header, data)
      end

      @doc ~S"""
      RECORD 11: HEAD RECORD FIELD2.
      """
      def parse_record(
            <<
              "11",
              _::binary-size(3),
              line_mark_code::binary-size(2),
              _::binary-size(3),
              vessel_code::binary-size(6),
              vessel_name::binary-size(20),
              voyage_number::binary-size(5),
              eta::binary-size(6),
              etd::binary-size(6),
              ship_registry_number::binary-size(15),
              call_sign::binary-size(6),
              _::binary-size(54)
            >>,
            acc,
            data
          ) do
        shipment_instruction = %{
          tag: 11,
          line_mark_code: ParserDataFormater.edi_string(line_mark_code),
          vessel_code: ParserDataFormater.edi_string(vessel_code),
          vessel_name: ParserDataFormater.edi_string(vessel_name),
          voyage_number: ParserDataFormater.edi_string(voyage_number),
          eta: ParserDataFormater.edi_optional_date(eta),
          etd: ParserDataFormater.edi_optional_date(etd),
          ship_registry_number: ParserDataFormater.edi_string(ship_registry_number),
          call_sign: ParserDataFormater.edi_string(call_sign)
        }

        acc
        |> ParserUpdater.update_shipment_instruction(shipment_instruction)
        |> callback("11", shipment_instruction, data)
      end

      @doc ~S"""
      RECORD 12: FIRST RECORD OF 1 B/L.
      """
      def parse_record(
            <<
              "12",
              _::binary-size(3),
              bol_number::binary-size(16),
              shipping_order_number::binary-size(16),
              pre_vessel_code::binary-size(6),
              pre_vessel_name::binary-size(20),
              pre_voyage_number::binary-size(5),
              place_of_receipt::binary-size(5),
              place_of_loading::binary-size(5),
              bol_cycfs_item::binary-size(9),
              bol_prepaid_or_collect_code::binary-size(1),
              bol_is_transshipment_port_code::binary-size(1),
              bol_all_empty_ctn_code::binary-size(1),
              loading_date::binary-size(6),
              shipping_and_carrier_number::binary-size(12),
              quarantine_coding::binary-size(1),
              _::binary-size(19)
            >>,
            acc,
            data
          ) do
        first_record_of_b_1 = %{
          tag: 12,
          bol_number: ParserDataFormater.edi_string(bol_number),
          shipping_order_number: ParserDataFormater.edi_string(shipping_order_number),
          pre_vessel_code: ParserDataFormater.edi_string(pre_vessel_code),
          pre_vessel_name: ParserDataFormater.edi_string(pre_vessel_name),
          pre_voyage_number: ParserDataFormater.edi_string(pre_voyage_number),
          place_of_receipt: ParserDataFormater.edi_string(place_of_receipt),
          place_of_loading: ParserDataFormater.edi_string(place_of_loading),
          bol_cycfs_item: ParserDataFormater.edi_string(bol_cycfs_item),
          bol_prepaid_or_collect_code: bol_prepaid_or_collect_code,
          bol_prepaid_or_collect:
            ParserDataFormater.explain_bol_poc_code(bol_prepaid_or_collect_code),
          bol_is_transshipment_port_code: bol_is_transshipment_port_code,
          bol_is_transshipment_port:
            ParserDataFormater.edi_boolean(bol_is_transshipment_port_code),
          bol_all_empty_ctn_code: bol_all_empty_ctn_code,
          bol_all_empty_ctn: ParserDataFormater.edi_boolean(bol_all_empty_ctn_code),
          loading_date: ParserDataFormater.edi_optional_date(loading_date),
          shipping_and_carrier_number: ParserDataFormater.edi_string(shipping_and_carrier_number),
          quarantine_coding: ParserDataFormater.edi_string(quarantine_coding)
        }

        ParserUpdater.update_first_record_of_b_1(acc, first_record_of_b_1)
        |> callback("12", first_record_of_b_1, data)
      end

      @doc ~S"""
      RECORD 13: PORT & PLACE  OF 1 B/L.
      """
      def parse_record(
            <<
              "13",
              _::binary-size(3),
              bol_port_of_discharge::binary-size(5),
              bol_port_of_delivery_code::binary-size(5),
              bol_port_of_delivery_name::binary-size(20),
              final_destination_code::binary-size(5),
              final_destination_name::binary-size(20),
              optional_port_1_code_type::binary-size(1),
              optional_port_1_code_left::binary-size(5),
              optional_port_1_code_right::binary-size(5),
              optional_port_2_code_type::binary-size(1),
              optional_port_2_code_left::binary-size(5),
              optional_port_2_code_right::binary-size(5),
              optional_port_3_code_type::binary-size(1),
              optional_port_3_code_left::binary-size(5),
              optional_port_3_code_right::binary-size(5),
              optional_port_4_code_type::binary-size(1),
              optional_port_4_code_left::binary-size(5),
              optional_port_4_code_right::binary-size(5),
              optional_port_5_code_type::binary-size(1),
              optional_port_5_code_left::binary-size(5),
              optional_port_5_code_right::binary-size(5),
              optional_port_6_code_type::binary-size(1),
              optional_port_6_code_left::binary-size(5),
              optional_port_6_code_right::binary-size(5),
              _::binary-size(2)
            >>,
            acc,
            data
          ) do
        optional_ports =
          [
            [optional_port_1_code_type, optional_port_1_code_left, optional_port_1_code_right],
            [optional_port_2_code_type, optional_port_2_code_left, optional_port_2_code_right],
            [optional_port_3_code_type, optional_port_3_code_left, optional_port_3_code_right],
            [optional_port_4_code_type, optional_port_4_code_left, optional_port_4_code_right],
            [optional_port_5_code_type, optional_port_5_code_left, optional_port_5_code_right],
            [optional_port_6_code_type, optional_port_6_code_left, optional_port_6_code_right]
          ]
          |> Enum.map(&ParserDataFormater.optional_port/1)
          |> Enum.reject(&is_nil/1)

        port_place = %{
          tag: 13,
          bol_port_of_discharge: ParserDataFormater.edi_string(bol_port_of_discharge),
          bol_port_of_delivery_code: ParserDataFormater.edi_string(bol_port_of_delivery_code),
          bol_port_of_delivery_name: ParserDataFormater.edi_string(bol_port_of_delivery_name),
          final_destination_code: ParserDataFormater.edi_string(final_destination_code),
          final_destination_name: ParserDataFormater.edi_string(final_destination_name),
          optional_ports: optional_ports
        }

        ParserUpdater.update_port_place(acc, port_place)
        |> callback("13", port_place, data)
      end

      @doc ~S"""
      RECORD 16: SHIPPER FIELDS.
      """
      def parse_record(
            <<
              "16",
              _::binary-size(3),
              shipper_code::binary-size(17),
              shipper_1::binary-size(35),
              shipper_2::binary-size(35),
              shipper_3::binary-size(35),
              _::binary-size(1)
            >>,
            acc,
            data
          ) do
        shipper_field = %{
          tag: 16,
          shipper_code: ParserDataFormater.edi_string(shipper_code),
          shipper_1: ParserDataFormater.edi_string(shipper_1),
          shipper_2: ParserDataFormater.edi_string(shipper_2),
          shipper_3: ParserDataFormater.edi_string(shipper_3)
        }

        ParserUpdater.update_shipper_field(acc, shipper_field)
        |> callback("16", shipper_field, data)
      end

      @doc ~S"""
      RECORD 21: CONSIGNEE FIELDS.
      """
      def parse_record(
            <<
              "21",
              _::binary-size(3),
              consignee_code::binary-size(17),
              consignee_1::binary-size(35),
              consignee_2::binary-size(35),
              consignee_3::binary-size(35),
              _::binary-size(1)
            >>,
            acc,
            data
          ) do
        consignee_field = %{
          tag: 21,
          consignee_code: ParserDataFormater.edi_string(consignee_code),
          consignee_1: ParserDataFormater.edi_string(consignee_1),
          consignee_2: ParserDataFormater.edi_string(consignee_2),
          consignee_3: ParserDataFormater.edi_string(consignee_3)
        }

        ParserUpdater.update_consignee_field(acc, consignee_field)
        |> callback("21", consignee_field, data)
      end

      @doc ~S"""
      RECORD 26: NOTIFY FIELDS.
      """
      def parse_record(
            <<
              "26",
              _::binary-size(3),
              notified_party_sequence_number::binary-size(1),
              notified_code::binary-size(17),
              notified_data_1::binary-size(35),
              notified_data_2::binary-size(35),
              notified_data_3::binary-size(35)
            >>,
            acc,
            data
          ) do
        notify_party_field = %{
          tag: 26,
          notified_party_sequence_number:
            ParserDataFormater.edi_integer(notified_party_sequence_number),
          notified_code: ParserDataFormater.edi_string(notified_code),
          notified_data_1: ParserDataFormater.edi_string(notified_data_1),
          notified_data_2: ParserDataFormater.edi_string(notified_data_2),
          notified_data_3: ParserDataFormater.edi_string(notified_data_3)
        }

        ParserUpdater.update_notify_party_field(acc, notify_party_field)
        |> callback("26", notify_party_field, data)
      end

      @doc ~S"""
      RECORD 41: CARGO FIELDS 1.
      """
      def parse_record(
            <<
              "41",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              cargo_code::binary-size(17),
              is_cargo_dangerous::binary-size(1),
              number_of_packages::binary-size(6),
              package_kind_code::binary-size(3),
              package_kind_description::binary-size(15),
              cargo_weight_gross::binary-size(10),
              cargo_weight_net::binary-size(10),
              cargo_measurement::binary-size(8),
              _::binary-size(28),
              unknown::binary-size(12),
              _::binary-size(10)
            >>,
            acc,
            data
          ) do
        cargo_description = %{
          cargo_fields: %{
            tag: 41,
            cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
            cargo_code: ParserDataFormater.edi_string(cargo_code),
            is_cargo_dangerous: ParserDataFormater.edi_boolean(is_cargo_dangerous),
            number_of_packages: ParserDataFormater.edi_integer(number_of_packages),
            package_kind_code: ParserDataFormater.edi_string(package_kind_code),
            package_kind_description: ParserDataFormater.edi_string(package_kind_description),
            cargo_weight_gross: ParserDataFormater.edi_decimal(cargo_weight_gross, 1),
            cargo_weight_net: ParserDataFormater.edi_decimal_nil_if_zero(cargo_weight_net, 1),
            cargo_measurement: ParserDataFormater.edi_decimal_nil_if_zero(cargo_measurement, 3),
            unknown: unknown
          },
          dangerous_cargo_desc: %{dangerous_cargo_49_field: %{}, dangerous_text: %{}},
          cargo_marks: [],
          cargo_descriptions: [],
          cargo_in_containers: []
        }

        ParserUpdater.update_cargo_fields(acc, cargo_description)
        |> callback("41", cargo_description, data)
      end

      @doc ~S"""
      RECORD 42: CFR 49 FIELD RECORD.
      """
      def parse_record(
            <<
              "42",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              cfr49_classification_1::binary-size(11),
              cfr49_classification_2::binary-size(11),
              label_1::binary-size(11),
              label_2::binary-size(11),
              emergency_contact::binary-size(20),
              _::binary-size(56)
            >>,
            acc,
            data
          ) do
        dangerous_cargo_49_field = %{
          tag: 42,
          cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
          emergency_contact: ParserDataFormater.edi_string(emergency_contact),
          cfr49_classification_1: ParserDataFormater.edi_string(cfr49_classification_1),
          cfr49_classification_2: ParserDataFormater.edi_string(cfr49_classification_2),
          label_1: ParserDataFormater.edi_string(label_1),
          label_2: ParserDataFormater.edi_string(label_2)
        }

        ParserUpdater.update_dangerous_cargo_49_field(acc, dangerous_cargo_49_field)
        |> callback("42", dangerous_cargo_49_field, data)
      end

      @doc ~S"""
      RECORD 43: DANGEROUS AND REEFER CARGO DESCRIPTION.
      """
      def parse_record(
            <<
              "43",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              class::binary-size(5),
              page::binary-size(7),
              un_number::binary-size(4),
              label_1::binary-size(16),
              label_2::binary-size(16),
              flash_point::binary-size(5),
              ems_number::binary-size(6),
              medical_first_aid_guide::binary-size(4),
              is_marine_pollutant::binary-size(1),
              temperature_from_sign::binary-size(1),
              temperature_from_value::binary-size(3),
              temperature_to_sign::binary-size(1),
              temperature_to_value::binary-size(3),
              temperature_measure::binary-size(1),
              _::binary-size(47)
            >>,
            acc,
            data
          ) do
        dangerous_text = %{
          tag: 43,
          cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
          class: ParserDataFormater.edi_string(class),
          page: ParserDataFormater.edi_string(page),
          un_number: ParserDataFormater.edi_string(un_number),
          label_1: label_1,
          label_2: label_2,
          flash_point: ParserDataFormater.edi_string(flash_point),
          ems_number: ParserDataFormater.edi_string(ems_number),
          medical_first_aid_guide: ParserDataFormater.edi_string(medical_first_aid_guide),
          is_marine_pollutant: ParserDataFormater.edi_boolean(is_marine_pollutant),
          temperature_from:
            ParserDataFormater.edi_signed_decimal(
              temperature_from_sign,
              temperature_from_value,
              1
            ),
          temperature_to:
            ParserDataFormater.edi_signed_decimal(temperature_to_sign, temperature_to_value, 1),
          temperature_measure_code: ParserDataFormater.edi_string(temperature_measure),
          temperature_measure:
            ParserDataFormater.explain_temperature_measure_code(
              ParserDataFormater.edi_string(temperature_measure)
            )
        }

        ParserUpdater.update_dangerous_text(acc, dangerous_text)
        |> callback("43", dangerous_text, data)
      end

      @doc ~S"""
      RECORD 44: CARGO MARKS AND NOS
      """
      def parse_record(
            <<
              "44",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              mark_1_key::binary-size(2),
              mark_1_value::binary-size(18),
              mark_2_key::binary-size(2),
              mark_2_value::binary-size(18),
              mark_3_key::binary-size(2),
              mark_3_value::binary-size(18),
              mark_4_key::binary-size(2),
              mark_4_value::binary-size(18),
              mark_5_key::binary-size(2),
              mark_5_value::binary-size(18),
              mark_6_key::binary-size(2),
              mark_6_value::binary-size(18)
            >>,
            acc,
            data
          ) do
        marks =
          [
            [mark_1_key, mark_1_value],
            [mark_2_key, mark_2_value],
            [mark_3_key, mark_3_value],
            [mark_4_key, mark_4_value],
            [mark_5_key, mark_5_value],
            [mark_6_key, mark_6_value]
          ]
          |> Enum.map(&ParserDataFormater.mark_pair/1)
          |> Enum.reject(&is_nil/1)

        cargo_mark = %{
          tag: 44,
          cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
          marks: marks
        }

        ParserUpdater.update_cargo_mark(acc, cargo_mark)
        |> callback("44", cargo_mark, data)
      end

      @doc ~S"""
      RECORD 47: CARGO DESCRIPTION.
      """
      def parse_record(
            <<
              "47",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              cargo_description_1::binary-size(30),
              cargo_description_2::binary-size(30),
              cargo_description_3::binary-size(30),
              cargo_description_4::binary-size(30)
            >>,
            acc,
            data
          ) do
        cargo_description = %{
          tag: 47,
          cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
          cargo_description_1: ParserDataFormater.edi_string(cargo_description_1),
          cargo_description_2: ParserDataFormater.edi_string(cargo_description_2),
          cargo_description_3: ParserDataFormater.edi_string(cargo_description_3),
          cargo_description_4: ParserDataFormater.edi_string(cargo_description_4)
        }

        ParserUpdater.update_cargo_description(acc, cargo_description)
        |> callback("47", cargo_description, data)
      end

      @doc ~S"""
      RECORD 51: CARGO IN CONTAINER.
      """
      def parse_record(
            <<
              "51",
              _::binary-size(3),
              cargo_sequence_number::binary-size(3),
              container_number::binary-size(11),
              is_shipper_owned_container::binary-size(1),
              seal_number::binary-size(10),
              container_size_and_type_iso_code::binary-size(4),
              is_dangerous_container::binary-size(1),
              container_loading_status_code::binary-size(1),
              container_cycfs_item::binary-size(9),
              is_container_short_shipped::binary-size(1),
              container_number_of_packages::binary-size(6),
              container_kind_of_packages::binary-size(8),
              container_cargo_weight_kg::binary-size(6),
              container_tare_weight_kg::binary-size(6),
              container_cargo_volume_m3::binary-size(8),
              stowage_location::binary-size(6),
              unknown::binary
            >>,
            acc,
            data
          ) do
        cargo_in_container = %{
          tag: 51,
          cargo_sequence_number: ParserDataFormater.edi_integer(cargo_sequence_number),
          container_number: ParserDataFormater.edi_string(container_number),
          is_shipper_owned_container: ParserDataFormater.edi_boolean(is_shipper_owned_container),
          seal_number: ParserDataFormater.edi_string(seal_number),
          container_size_and_type_iso_code:
            ParserDataFormater.edi_string(container_size_and_type_iso_code),
          is_dangerous_container: ParserDataFormater.edi_boolean(is_dangerous_container),
          container_loading_status_code:
            ParserDataFormater.edi_string(container_loading_status_code),
          container_loading_status:
            ParserDataFormater.explain_container_loading_status_code(
              ParserDataFormater.edi_string(container_loading_status_code)
            ),
          container_cycfs_item: ParserDataFormater.edi_string(container_cycfs_item),
          is_container_short_shipped: ParserDataFormater.edi_boolean(is_container_short_shipped),
          container_number_of_packages:
            ParserDataFormater.edi_integer(container_number_of_packages),
          container_kind_of_packages: ParserDataFormater.edi_string(container_kind_of_packages),
          container_cargo_weight_kg: ParserDataFormater.edi_decimal(container_cargo_weight_kg, 1),
          container_tare_weight_kg: ParserDataFormater.edi_decimal(container_tare_weight_kg, 1),
          container_cargo_volume_m3: ParserDataFormater.edi_decimal(container_cargo_volume_m3, 3),
          stowage_location: ParserDataFormater.edi_string(stowage_location),
          unknown: ParserDataFormater.edi_string(unknown)
        }

        ParserUpdater.update_cargo_in_container(acc, cargo_in_container)
        |> callback("51", cargo_in_container, data)
      end

      @doc ~S"""
      RECORD 61: FREIGHT AND CHARGES.
      """
      def parse_record(
            <<
              "61",
              _::binary-size(3),
              sequence_number::binary-size(2),
              freight_charge_code::binary-size(3),
              freight_charge_remark::binary-size(35),
              payable_at::binary-size(5),
              quantity::binary-size(9),
              currency::binary-size(3),
              rate_of_freight_charge::binary-size(13),
              freight_unit_of_quantity::binary-size(4),
              amount::binary-size(13),
              amount_sign::binary-size(1),
              exchange_rate::binary-size(12),
              exchange_to_currency::binary-size(3),
              equivalent_amount::binary-size(13),
              equivalent_amount_sign::binary-size(1),
              prepaid_or_collect_code::binary-size(1),
              unknown::binary-size(5)
            >>,
            acc,
            data
          ) do
        pocc = ParserDataFormater.edi_string(prepaid_or_collect_code)

        freight_information = %{
          tag: 61,
          sequence_number: ParserDataFormater.edi_integer(sequence_number),
          freight_charge_code: ParserDataFormater.edi_string(freight_charge_code),
          freight_charge_remark: ParserDataFormater.edi_string(freight_charge_remark),
          payable_at: ParserDataFormater.edi_string(payable_at),
          quantity: ParserDataFormater.edi_decimal(quantity, 3),
          currency: ParserDataFormater.edi_string(currency),
          rate_of_freight_charge: ParserDataFormater.edi_decimal(rate_of_freight_charge, 3),
          freight_unit_of_quantity: ParserDataFormater.edi_string(freight_unit_of_quantity),
          amount: ParserDataFormater.edi_signed_decimal(amount_sign, amount, 3),
          exchange_rate: ParserDataFormater.edi_decimal(exchange_rate, 8),
          exchange_to_currency: ParserDataFormater.edi_string(exchange_to_currency),
          equivalent_amount:
            ParserDataFormater.edi_signed_decimal(equivalent_amount_sign, equivalent_amount, 3),
          prepaid_or_collect_code: pocc,
          prepaid_or_collect: ParserDataFormater.explain_freight_poc_code(pocc),
          unknown: ParserDataFormater.edi_string(unknown)
        }

        ParserUpdater.update_freight_information(acc, freight_information)
        |> callback("61", freight_information, data)
      end

      @doc ~S"""
      RECORD 71: OTHER FIELDS.
      """
      def parse_record(
            <<
              "71",
              _::binary-size(3),
              up_remark_1::binary-size(35),
              up_remark_2::binary-size(35),
              up_remark_3::binary-size(35),
              _::binary-size(18)
            >>,
            acc,
            data
          ) do
        other_field_1 = %{
          tag: 71,
          up_remark_1: ParserDataFormater.edi_string(up_remark_1),
          up_remark_2: ParserDataFormater.edi_string(up_remark_2),
          up_remark_3: ParserDataFormater.edi_string(up_remark_3)
        }

        ParserUpdater.update_other_field_1(acc, other_field_1)
        |> callback("71", other_field_1, data)
      end

      @doc ~S"""
      RECORD 72: OTHER FIELDS.
      """
      def parse_record(
            <<
              "72",
              _::binary-size(3),
              down_remark_1::binary-size(35),
              down_remark_2::binary-size(35),
              down_remark_3::binary-size(35),
              _::binary-size(18)
            >>,
            acc,
            data
          ) do
        other_field_2 = %{
          tag: 71,
          down_remark_1: ParserDataFormater.edi_string(down_remark_1),
          down_remark_2: ParserDataFormater.edi_string(down_remark_2),
          down_remark_3: ParserDataFormater.edi_string(down_remark_3)
        }

        ParserUpdater.update_other_field_2(acc, other_field_2)
        |> callback("71", other_field_2, data)
      end

      @doc ~S"""
      RECORD 73: IN WORDS.
      """
      def parse_record(
            <<
              "73",
              _::binary-size(3),
              in_words_1::binary-size(56),
              in_words_2::binary-size(56),
              _::binary-size(11)
            >>,
            acc,
            data
          ) do
        in_word = %{
          tag: 73,
          in_words_1: in_words_1,
          in_words_2: in_words_2
        }

        ParserUpdater.update_in_word(acc, in_word)
        |> callback("73", in_word, data)
      end

      @doc ~S"""
      RECORD 74: FIELDS NOT IN MANIFEST.
      """
      def parse_record(
            <<
              "74",
              _::binary-size(3),
              bol_place_of_issue::binary-size(5),
              bol_place_and_date_of_issue::binary-size(30),
              prepaid_at::binary-size(20),
              payable_at::binary-size(20),
              _::binary-size(48)
            >>,
            acc,
            data
          ) do
        fields_not_in_manifest = %{
          tag: 74,
          bol_place_of_issue: ParserDataFormater.edi_string(bol_place_of_issue),
          bol_place_and_date_of_issue: ParserDataFormater.edi_string(bol_place_and_date_of_issue),
          prepaid_at: ParserDataFormater.edi_string(prepaid_at),
          payable_at: ParserDataFormater.edi_string(payable_at)
        }

        ParserUpdater.update_fields_not_in_manifest(acc, fields_not_in_manifest)
        |> callback("74", fields_not_in_manifest, data)
      end

      @doc ~S"""
      RECORD 99: TRAILER.
      Must appear exactly once.
      """
      def parse_record(
            <<
              "99",
              _::binary-size(3),
              number_of_records::binary-size(6),
              _::binary-size(117)
            >>,
            acc,
            data
          ) do
        trailer = %{
          tag: 99,
          number_of_records: ParserDataFormater.edi_integer(number_of_records)
        }

        ParserUpdater.update_trailer(acc, trailer)
        |> callback("99", trailer, data)
      end

      def parse_record("", acc, data),
        do: acc

      def parse_record(row, _acc, data),
        do: Logger.error("Row: #{row} is not parsed!!!")

      def parse_edi_text(text, data)
          when is_binary(text) do
        acc = %{
          header: %{},
          shipment_instruction: %{},
          shipment_informations: [],
          trailer: %{}
        }

        document =
          text
          |> String.split(["\r\n", "\n"])
          |> Enum.reduce(acc, fn x, acc -> parse_record(x, acc, data) end)

        sorted_shipment_informations =
          Enum.sort_by(
            document.shipment_informations,
            &get_in(&1, [:first_record_of_b_1, :port, :name])
          )

        %{document | shipment_informations: sorted_shipment_informations}
      end

      def callback(acc, _data_type, _parsed_data, _data) do
        acc
      end

      defoverridable callback: 4
    end
  end
end
