defmodule AuthMessenger.Auth do
  alias AuthMessenger.DefaultResponse

  @default_response DefaultResponse

  defmodule AuthenticateRequest do
    @keys [:email, :password]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  defmodule ValidateTokenRequest do
    @keys [:token]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec authenticate(%AuthenticateRequest{email: String.t(), password: String.t()}, []) ::
          %DefaultResponse{}
  def authenticate(%AuthenticateRequest{email: _, password: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "auth/authenticate", payload, opts)

    struct(@default_response, response)
  end

  @spec validate_token(%ValidateTokenRequest{token: String.t()}, []) :: %DefaultResponse{}
  def validate_token(%ValidateTokenRequest{token: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "auth/validate", payload, opts)

    struct(@default_response, response)
  end
end
