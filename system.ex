defmodule AuthMessenger.System do
  alias AuthMessenger.DefaultResponse

  @default_response DefaultResponse

  defmodule CreateRequest do
    @keys [:name]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec create(
          %CreateRequest{
            name: String.t()
          },
          []
        ) ::
          %DefaultResponse{}
  def create(
        %CreateRequest{name: _} = payload,
        opts \\ []
      ) do
    response = AuthMessenger.send("request/auth_service", "system/create", payload, opts)

    struct(@default_response, response)
  end

  defmodule UpdateRequest do
    @keys [:id, :name]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec update(
          %UpdateRequest{
            id: Integer.t(),
            name: String.t()
          },
          []
        ) ::
          %DefaultResponse{}
  def update(
        %UpdateRequest{id: _, name: _} = payload,
        opts \\ []
      ) do
    response = AuthMessenger.send("request/auth_service", "system/update", payload, opts)

    struct(@default_response, response)
  end

  defmodule GetRequest do
    @keys [:id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec get(%GetRequest{id: Integer.t()}, []) :: %DefaultResponse{}
  def get(%GetRequest{id: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "system/get", payload, opts)

    struct(@default_response, response)
  end

  def all(opts \\ []) do
    AuthMessenger.send("request/auth_service", "system/all", "", opts)
  end

  defmodule DeleteRequest do
    @keys [:id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec delete(%DeleteRequest{id: Integer.t()}, []) :: %DefaultResponse{}
  def delete(%DeleteRequest{id: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "system/delete", payload, opts)

    struct(@default_response, response)
  end
end
