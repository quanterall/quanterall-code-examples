defmodule AuthMessenger.User do
  alias AuthMessenger.DefaultResponse

  @default_response DefaultResponse

  defmodule CreateRequest do
    @keys [:email, :password, :password_confirmation, :system_id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec create(
          %CreateRequest{
            email: String.t(),
            password: String.t(),
            password_confirmation: String.t(),
            system_id: Integer.t()
          },
          []
        ) ::
          %DefaultResponse{}
  def create(
        %CreateRequest{email: _, password: _, password_confirmation: _, system_id: _} = payload,
        opts \\ []
      ) do
    response = AuthMessenger.send("request/auth_service", "user/create", payload, opts)

    struct(@default_response, response)
  end

  defmodule UpdateRequest do
    @keys [:id, :email, :system_id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec update(
          %UpdateRequest{
            id: Integer.t(),
            email: String.t(),
            system_id: Integer.t()
          },
          []
        ) ::
          %DefaultResponse{}
  def update(
        %UpdateRequest{id: _, email: _, system_id: _} =
          payload,
        opts \\ []
      ) do
    response = AuthMessenger.send("request/auth_service", "user/update", payload, opts)

    struct(@default_response, response)
  end

  defmodule GetRequest do
    @keys [:id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec get(%GetRequest{id: Integer.t()}, []) :: %DefaultResponse{}
  def get(%GetRequest{id: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "user/get", payload, opts)

    struct(@default_response, response)
  end

  def all(opts \\ []) do
    AuthMessenger.send("request/auth_service", "user/all", "", opts)
  end

  defmodule DeleteRequest do
    @keys [:id]
    @enforce_keys @keys
    @derive {Jason.Encoder, only: @keys}
    defstruct @keys
  end

  @spec delete(%DeleteRequest{id: Integer.t()}, []) :: %DefaultResponse{}
  def delete(%DeleteRequest{id: _} = payload, opts \\ []) do
    response = AuthMessenger.send("request/auth_service", "user/delete", payload, opts)

    struct(@default_response, response)
  end
end
