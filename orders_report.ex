defmodule Report.Order do
  import Ecto.Query, warn: false
  alias {Repo, Order}

  def daily_total() do
    from(o in Order,
      select: %{count: count(o.id), sum: sum(o.price)},
      where: fragment("?::DATE = CURRENT_DATE", o.inserted_at)
    )
    |> Repo.one()
  end

  def weekly_total() do
    from(o in Order,
      select: %{count: count(o.id), sum: sum(o.price)},
      where: fragment("?::DATE >= CURRENT_DATE - interval '7' day", o.inserted_at)
    )
    |> Repo.one()
  end

  def weekly_by_product_info() do
    by_product_info_query()
    |> where([o, _p, _pi], fragment("?::DATE >= CURRENT_DATE - interval '7' day", o.inserted_at))
    |> Repo.all()
  end

  def daily_by_product_info() do
    by_product_info_query()
    |> where([o, _p, _pi], fragment("?::DATE = CURRENT_DATE", o.inserted_at))
    |> Repo.all()
  end

  def by_product_info_query() do
    from(o in Order,
      join: p in assoc(o, :product),
      join: pi in assoc(p, :product_info),
      select: %{count: count(p.id), name: pi.name},
      group_by: pi.id,
      order_by: count(o.id)
    )
  end

  def weekly_by_location() do
    by_location_query()
    |> where([o, l], fragment("?::DATE >= CURRENT_DATE - interval '7' day", o.inserted_at))
    |> Repo.all()
  end

  def daily_by_location() do
    by_location_query()
    |> where([o, l], fragment("?::DATE = CURRENT_DATE", o.inserted_at))
    |> Repo.all()
  end

  def by_location_query() do
    from(o in Order,
      join: l in assoc(o, :location),
      select: %{count: count(o.id), name: l.name},
      group_by: l.id,
      order_by: count(o.id)
    )
  end

  def get_orders_by_day() do
    from(o in Order,
      select: %{
        count: count(o.id),
        amount: sum(o.price),
        date: fragment("?::DATE", o.inserted_at)
      },
      group_by: fragment("?::DATE", o.inserted_at),
      where:
        fragment(
          "?::DATE >= CURRENT_DATE - interval '7' day AND ?::DATE <= CURRENT_DATE",
          o.inserted_at,
          o.inserted_at
        )
    )
    |> Repo.all()
  end

  def get_orders_by_location() do
    from(o in Order,
      join: l in assoc(o, :location),
      select: %{count: count(o.id), location: l.name},
      group_by: l.id
    )
    |> Repo.all()
  end
end
