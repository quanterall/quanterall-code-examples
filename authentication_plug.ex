defmodule Authenticated.Plug do
  import Plug.Conn

  @salt Application.get_env(:three_star, Endpoint)[:secret_key_base]
  def init(_params) do
  end

  def call(conn, _params) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> token] ->
        case Phoenix.Token.verify(Endpoint, @salt, token, max_age: 86400) do
          {:ok, user} ->
            assign(conn, :current_user, user)

          {:error, msg} ->
            conn
            |> put_status(:unauthorized)
            |> json(%{error: "Token #{msg}"})
            |> halt()
        end

      _ ->
        conn
        |> put_status(:unauthorized)
        |> json(%{error: "You need to sign in or sign up before continuing."})
        |> halt()
    end
  end
end
