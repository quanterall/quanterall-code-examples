defmodule ClientMessenger do
  use GenServer

  def start_link(
        %{client_id: _client, respond_to: _respond_to, genserver_name: genserver_name} =
          init_state
      ) do
    GenServer.start_link(__MODULE__, init_state |> Map.put(:awaiters, %{}), name: genserver_name)
  end

  def init(data) do
    {:ok, data}
  end

  def handle_call(
        {:send, {route, action, user, payload, opts}},
        from,
        %{awaiters: awaiters, client_id: client, respond_to: respond_to} = state
      ) do
    uuid = UUID.uuid1()
    respond_to = Keyword.get(opts, :respond_to, respond_to)

    :ok =
      Tortoise.publish(
        client,
        route,
        Jason.encode!(%{
          uuid: uuid,
          payload: payload,
          user: user,
          action: action,
          respond_to: respond_to
        }),
        qos: 0
      )

    {:noreply, %{state | awaiters: Map.put(awaiters, uuid, from)}}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:get_client, _from, %{client_id: client} = state) do
    {:reply, client, state}
  end

  def handle_cast(
        {:receive, uuid, status, payload},
        %{
          awaiters: awaiters
        } = state
      ) do
    with {:ok, pid} <- Map.fetch(awaiters, uuid) do
      GenServer.reply(pid, %{status: status, payload: payload})
    end

    {:noreply, %{state | awaiters: Map.delete(awaiters, uuid)}}
  end

  def send(genserver_name, route, action, user, payload, opts) do
    try do
      GenServer.call(genserver_name, {:send, {route, action, user, payload, opts}}, 5000)
    catch
      :exit, _ -> %{status: "error", payload: %{"error_code" => "timeout_service_not_available"}}
    end
  end

  def receive(
        gen_server,
        _topic,
        %{"payload" => payload, "status" => status, "uuid" => uuid} = _decoded_payload
      ) do
    GenServer.cast(gen_server, {:receive, uuid, status, payload})
  end

  def state() do
    GenServer.call(__MODULE__, :get_state)
  end

  def client() do
    GenServer.call(__MODULE__, :get_client)
  end
end
