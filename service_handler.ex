defmodule ServiceHandler do
  alias Service.{
    UserController,
    MachineController,
    ServiceMessenger
  }

  @paths %{
    "user/create" => %{module: UserController, action: :create},
    "user/update" => %{module: UserController, action: :update}
  }

  def handle_message(
        [_h | _t] = _topic,
        %{"action" => path, "payload" => payload, "user" => %{"company_id" => company_id} = user} =
          request_payload
      ) do
    Service.Repo.as_company(to_string(company_id), fn ->
      :erlang.apply(@paths[path].module, @paths[path].action, [payload, user])
      |> respond(request_payload)
    end)
  end

  def handle_message(
        [_h | _t] = _topic,
        %{"action" => path, "payload" => payload} = request_payload
      ) do
    Service.Repo.with_disabled_restriction(fn ->
      :erlang.apply(@paths[path].module, @paths[path].action, [payload])
      |> respond(request_payload)
    end)
  end

  def respond({:ok, response_payload}, request_payload) do
    ServiceMessenger.respond(request_payload, :success, response_payload)
  end

  def respond({:error, _reason} = error, request_payload) do
    ServiceMessenger.respond(request_payload, :error, ResponseFormatter.format(error))
  end
end
