defmodule AuthService.DefaultModel do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
      import Ecto.Query, warn: false
      alias AuthService.Repo

      def create(attrs) do
        struct(__MODULE__, %{})
        |> changeset(attrs)
        |> Repo.insert()
      end

      def update(id, params) do
        with {:ok, entry} <- get(id),
             {:ok, result} <- Repo.update(changeset(entry, params)) do
          {:ok, result}
        else
          error ->
            error
        end
      end

      def all(preloads \\ []) do
        users =
          __MODULE__
          |> Repo.all()
          |> Repo.preload(preloads)

        {:ok, users}
      end

      def get(id) do
        with entry when not is_nil(entry) <- Repo.get(__MODULE__, id) do
          {:ok, entry}
        else
          _ ->
            {:error, "#{format_module_name()}_not_found"}
        end
      end

      def format_module_name() do
        name =
          __MODULE__
          |> Atom.to_string()
          |> String.split(".")
          |> List.last()

        String.last(name)
        |> case do
          "s" -> String.slice(name, 0..-2)
          _ -> name
        end
        |> String.downcase()
      end

      def get_by(search_terms, preloads \\ []) do
        with entry when not is_nil(entry) <- build_query(search_terms, preloads) |> Repo.one() do
          {:ok, entry}
        else
          _ ->
            {:error, "#{format_module_name()}_not_found"}
        end
      end

      def get_all_by(search_terms, preloads \\ []) do
        build_query(search_terms, preloads) |> Repo.all()
      end

      def delete(id) do
        with {:ok, entry} <- get(id),
             {:ok, entry} <- Repo.delete(__MODULE__.changeset(entry)) do
          {:ok, entry}
        else
          error ->
            error
        end
      end

      defp build_query(search_terms, preloads) do
        Enum.reduce(
          search_terms,
          from(item in __MODULE__, preload: ^preloads),
          fn
            {k, v}, q when is_list(v) ->
              where(q, [i], field(i, ^convert_field_name(k)) in ^v)

            {k, ""}, q ->
              q

            {k, v}, q ->
              where(q, [i], field(i, ^convert_field_name(k)) == ^v)
          end
        )
      end

      defp convert_field_name(field) when is_atom(field), do: field
      defp convert_field_name(field) when is_binary(field), do: String.to_atom(field)
    end
  end
end
