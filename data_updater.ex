defmodule Edi.ParserUpdater do
  def update_header(acc, header) do
    %{acc | header: header}
  end

  def update_shipment_instruction(acc, shipment_instruction) do
    %{
      acc
      | shipment_instruction: shipment_instruction
    }
  end

  def update_first_record_of_b_1(acc, first_record_of_b_1) do
    %{shipment_informations: shipment_informations} = acc

    first_record_of_b_1 =
      Map.put(first_record_of_b_1, :port, %{id: nil, code: nil, name: nil, country: nil})

    %{
      acc
      | shipment_informations: [
          %{
            first_record_of_b_1: first_record_of_b_1,
            port_place: %{},
            parties_concern: %{
              shipper_fields: [],
              consignee_fields: [],
              notify_party_fields: []
            },
            client_info: nil,
            cargo_descriptions: [],
            freight_informations: [],
            information_for_reference: %{
              other_fields1: [],
              other_fields2: [],
              in_words: [],
              fields_not_in_manifest: %{}
            }
          }
          | shipment_informations
        ]
    }
  end

  def update_port_place(acc, port_place) do
    %{shipment_informations: [first_shipment_informations | tail]} = acc

    %{
      acc
      | shipment_informations: [
          %{first_shipment_informations | port_place: port_place}
          | tail
        ]
    }
  end

  def update_shipper_field(acc, shipper_field) do
    %{
      shipment_informations: [
        %{parties_concern: %{shipper_fields: shipper_fields} = parties_concern} =
          first_shipment_informations
        | tail
      ]
    } = acc

    previews_shipper_fields =
      case shipper_fields do
        [prev] ->
          prev

        _ ->
          %{parsed_data: %{shipper_1: ""}}
      end

    shipper_field = %{
      parsed_data: %{
        shipper_1:
          "#{shipper_field.shipper_1} #{shipper_field.shipper_2} #{shipper_field.shipper_3}" <>
            previews_shipper_fields.parsed_data.shipper_1
      },
      shipper_code: shipper_field.shipper_code,
      clients_id: nil,
      tag: 16
    }

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | parties_concern: %{
                parties_concern
                | shipper_fields: [shipper_field]
              }
          }
          | tail
        ]
    }
  end

  def update_consignee_field(acc, consignee_field) do
    %{
      shipment_informations: [
        %{parties_concern: %{consignee_fields: consignee_fields} = parties_concern} =
          first_shipment_informations
        | tail
      ]
    } = acc

    previews_consignee =
      case consignee_fields do
        [prev] ->
          prev

        _ ->
          %{parsed_data: %{consignee_1: ""}}
      end

    consignee_field = %{
      parsed_data: %{
        consignee_1:
          "#{consignee_field.consignee_1} #{consignee_field.consignee_2} #{
            consignee_field.consignee_3
          }" <> previews_consignee.parsed_data.consignee_1
      },
      consignee_code: consignee_field.consignee_code,
      clients_id: nil,
      tag: consignee_field.tag
    }

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | parties_concern: %{
                parties_concern
                | consignee_fields: [consignee_field]
              }
          }
          | tail
        ]
    }
  end

  def update_notify_party_field(acc, notify_party_field) do
    %{
      shipment_informations: [
        %{parties_concern: %{notify_party_fields: notify_party_fields} = parties_concern} =
          first_shipment_informations
        | tail
      ]
    } = acc

    previews_notify_party =
      case notify_party_fields do
        [prev] ->
          prev

        _ ->
          %{parsed_data: %{notified_data_1: ""}}
      end

    notify_party_field = %{
      parsed_data: %{
        notified_data_1:
          "#{notify_party_field.notified_data_1} #{notify_party_field.notified_data_2} #{
            notify_party_field.notified_data_3
          }" <> previews_notify_party.parsed_data.notified_data_1
      },
      notified_code: notify_party_field.notified_code,
      notified_party_sequence_number: notify_party_field.notified_party_sequence_number,
      clients_id: nil,
      tag: notify_party_field.tag
    }

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | parties_concern: %{
                parties_concern
                | notify_party_fields: [notify_party_field]
              }
          }
          | tail
        ]
    }
  end

  def update_cargo_fields(acc, cargo_description) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: cargo_descriptions
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [cargo_description | cargo_descriptions]
          }
          | tail
        ]
    }
  end

  def update_dangerous_cargo_49_field(acc, dangerous_cargo_49_field) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: [
            %{dangerous_cargo_desc: %{} = dangerous_cargo_desc} = fisrt_cargo_descriptions
            | cargo_descriptions
          ]
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [
                %{
                  fisrt_cargo_descriptions
                  | dangerous_cargo_desc: %{
                      dangerous_cargo_desc
                      | dangerous_cargo_49_field: dangerous_cargo_49_field
                    }
                }
                | cargo_descriptions
              ]
          }
          | tail
        ]
    }
  end

  def update_dangerous_text(acc, dangerous_text) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: [
            %{dangerous_cargo_desc: %{} = dangerous_cargo_desc} = fisrt_cargo_descriptions
            | cargo_descriptions
          ]
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [
                %{
                  fisrt_cargo_descriptions
                  | dangerous_cargo_desc: %{
                      dangerous_cargo_desc
                      | dangerous_text: dangerous_text
                    }
                }
                | cargo_descriptions
              ]
          }
          | tail
        ]
    }
  end

  def update_cargo_mark(acc, cargo_mark) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: [
            %{cargo_marks: cargo_marks} = fisrt_cargo_descriptions
            | cargo_descriptions
          ]
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [
                %{
                  fisrt_cargo_descriptions
                  | cargo_marks: [cargo_mark | cargo_marks]
                }
                | cargo_descriptions
              ]
          }
          | tail
        ]
    }
  end

  def update_cargo_description(acc, cargo_description) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: [
            %{cargo_descriptions: inner_cargo_descriptions} = fisrt_cargo_descriptions
            | cargo_descriptions
          ]
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [
                %{
                  fisrt_cargo_descriptions
                  | cargo_descriptions: [cargo_description | inner_cargo_descriptions]
                }
                | cargo_descriptions
              ]
          }
          | tail
        ]
    }
  end

  def update_cargo_in_container(acc, cargo_in_container) do
    %{
      shipment_informations: [
        %{
          cargo_descriptions: [
            %{cargo_in_containers: cargo_in_containers} = fisrt_cargo_descriptions
            | cargo_descriptions
          ]
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | cargo_descriptions: [
                %{
                  fisrt_cargo_descriptions
                  | cargo_in_containers: [cargo_in_container | cargo_in_containers]
                }
                | cargo_descriptions
              ]
          }
          | tail
        ]
    }
  end

  def update_freight_information(acc, freight_information) do
    %{
      shipment_informations: [
        %{
          freight_informations: freight_informations
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | freight_informations: [
                freight_information
                | freight_informations
              ]
          }
          | tail
        ]
    }
  end

  def update_other_field_1(acc, other_field_1) do
    %{
      shipment_informations: [
        %{
          information_for_reference: %{other_fields1: other_fields_1} = information_for_reference
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | information_for_reference: %{
                information_for_reference
                | other_fields1: [other_field_1 | other_fields_1]
              }
          }
          | tail
        ]
    }
  end

  def update_other_field_2(acc, other_field_2) do
    %{
      shipment_informations: [
        %{
          information_for_reference: %{other_fields2: other_fields_2} = information_for_reference
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | information_for_reference: %{
                information_for_reference
                | other_fields2: [other_field_2 | other_fields_2]
              }
          }
          | tail
        ]
    }
  end

  def update_in_word(acc, in_word) do
    %{
      shipment_informations: [
        %{
          information_for_reference: %{in_words: in_words} = information_for_reference
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | information_for_reference: %{
                information_for_reference
                | in_words: [in_word | in_words]
              }
          }
          | tail
        ]
    }
  end

  def update_fields_not_in_manifest(acc, fields_not_in_manifest) do
    %{
      shipment_informations: [
        %{
          information_for_reference: information_for_reference
        } = first_shipment_informations
        | tail
      ]
    } = acc

    %{
      acc
      | shipment_informations: [
          %{
            first_shipment_informations
            | information_for_reference: %{
                information_for_reference
                | fields_not_in_manifest: fields_not_in_manifest
              }
          }
          | tail
        ]
    }
  end

  def update_trailer(acc, trailer) do
    %{acc | trailer: trailer}
  end
end
